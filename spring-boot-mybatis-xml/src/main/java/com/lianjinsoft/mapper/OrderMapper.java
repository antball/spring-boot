package com.lianjinsoft.mapper;

import java.util.List;

import com.lianjinsoft.pojo.Order;

public interface OrderMapper {
	List<Order> queryOrders();
	
	Order queryOrderByNo(String orderNo);
	
	void addOrder(Order order);
	
	void delOrder(Integer id);
	
	void updOrder(Order order);
}
