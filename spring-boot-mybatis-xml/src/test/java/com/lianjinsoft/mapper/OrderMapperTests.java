package com.lianjinsoft.mapper;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lianjinsoft.pojo.Order;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderMapperTests {
	@Autowired
	private OrderMapper orderMapper;

	@Test
	public void test1_Add() {
		orderMapper.addOrder(new Order("100001", "XX手机Plus 全面屏", new BigDecimal("4999")));
	}
	
	@Test
	public void test2_Query() {
		Order order = orderMapper.queryOrderByNo("100001");
		Assert.assertNotNull(order);
	}
	
	@Test
	public void test3_QueryOrders() {
		Assert.assertEquals(1, orderMapper.queryOrders().size());
	}
	
	@Test
	public void test4_Upd() {
		Order order = orderMapper.queryOrderByNo("100001");
		order.setOrderName("三星S8+ 全面屏");
		
		orderMapper.updOrder(order);
	}
	
	@Test
	public void test5_Del() {
		orderMapper.delOrder(orderMapper.queryOrders().get(0).getId());
		
		Assert.assertEquals(0, orderMapper.queryOrders().size());
	}
	
}
