### SpringBoot项目简介
本项目包含springboot主流项目示例，帮助新手快速入门、搭建相关应用环境。<br/>
本人将不定期更新以下列举的项目Demo。<br/>
<br/>
-- **Spring Boot Mybatis** <br/>
集成Mybatis<br/>
<br/>
-- **Spring Boot Jpa** <br/>
集成Hibernate<br/>
<br/>
-- **Spring Boot Redis** <br/>
集成Redis<br/>
<br/>
-- **Spring Boot MQ** <br/>
集成RabbitMQ<br/>
<br/>
-- **Spring Boot Schedule** <br/>
开启定时任务<br/>
<br/>
-- **Spring Boot Email** <br/>
集成Email服务<br/>



