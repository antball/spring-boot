package com.lianjinsoft.sender;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lianjinsoft.pojo.Order;
import com.lianjinsoft.util.LogUtil;

@Component
public class RefundNotifySender {
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	public void sender(Order order){
		LogUtil.info("notify.refund send message: "+order);
		rabbitTemplate.convertAndSend("notify.refund", order);
	}
}
