package com.lianjinsoft.sender;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lianjinsoft.pojo.Order;
import com.lianjinsoft.util.LogUtil;

@Component
public class QueryOrderSender {
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	public void sender(String orderId){
		LogUtil.info("query.order send message: "+orderId);
		Order order = (Order) rabbitTemplate.convertSendAndReceive("query.order", orderId);
		LogUtil.info("query.order return message: "+order);
	}
}
