package com.lianjinsoft.sender;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lianjinsoft.util.LogUtil;

@Component
public class ApiCoreSender {
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	public void user(String msg){
		LogUtil.info("api.core.user send message: "+msg);
		rabbitTemplate.convertAndSend("coreExchange", "api.core.user", msg);
	}
	
	public void userQuery(String msg){
		LogUtil.info("api.core.user.query send message: "+msg);
		rabbitTemplate.convertAndSend("coreExchange", "api.core.user.query", msg);
	}
}
