package com.lianjinsoft.sender;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lianjinsoft.util.LogUtil;

@Component
public class PaymentNotifySender {
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	public void sender(String msg){
		LogUtil.info("notify.payment send message: "+msg);
		rabbitTemplate.convertAndSend("notify.payment", msg);
	}
}
