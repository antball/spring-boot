package com.lianjinsoft.sender;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lianjinsoft.util.LogUtil;

@Component
public class ApiPaymentSender {
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	public void order(String msg){
		LogUtil.info("api.payment.order send message: "+msg);
		rabbitTemplate.convertAndSend("paymentExchange", "api.payment.order", msg);
	}
	
	public void orderQuery(String msg){
		LogUtil.info("api.payment.order.query send message: "+msg);
		rabbitTemplate.convertAndSend("paymentExchange", "api.payment.order.query", msg);
	}
	
	public void orderDetailQuery(String msg){
		LogUtil.info("api.payment.order.detail.query send message: "+msg);
		rabbitTemplate.convertAndSend("paymentExchange", "api.payment.order.detail.query", msg);
	}
}
