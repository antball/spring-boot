package com.lianjinsoft.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.lianjinsoft.util.LogUtil;

@Component
public class ApiReportReceive {
	@RabbitHandler
	@RabbitListener(queues = "api.report.payment")
	public void payment(String msg) {
		LogUtil.info("api.report.payment receive message: "+msg);
	}
	
	@RabbitHandler
	@RabbitListener(queues = "api.report.refund")
	public void refund(String msg) {
		LogUtil.info("api.report.refund receive message: "+msg);
	}
}
