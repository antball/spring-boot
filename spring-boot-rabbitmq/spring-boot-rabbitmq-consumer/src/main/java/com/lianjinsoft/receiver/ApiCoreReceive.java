package com.lianjinsoft.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.lianjinsoft.util.LogUtil;

@Component
public class ApiCoreReceive {
	@RabbitHandler
	@RabbitListener(queues = "api.core")
	public void user(String msg) {
		LogUtil.info("api.core receive message: "+msg);
	}
}
