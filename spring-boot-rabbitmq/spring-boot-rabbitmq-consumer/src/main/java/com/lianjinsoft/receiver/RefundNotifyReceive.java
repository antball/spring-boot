package com.lianjinsoft.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.lianjinsoft.pojo.Order;
import com.lianjinsoft.util.LogUtil;

@Component
@RabbitListener(queues = "notify.refund")
public class RefundNotifyReceive {
	@RabbitHandler
	public void receive(Order order) {
		LogUtil.info("notify.refund receive message: "+order);
	}
}
