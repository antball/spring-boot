package com.lianjinsoft.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.lianjinsoft.util.LogUtil;

@Component
public class ApiPaymentReceive {
	@RabbitHandler
	@RabbitListener(queues = "api.payment")
	public void order(String msg) {
		LogUtil.info("api.payment.order receive message: "+msg);
	}
}
