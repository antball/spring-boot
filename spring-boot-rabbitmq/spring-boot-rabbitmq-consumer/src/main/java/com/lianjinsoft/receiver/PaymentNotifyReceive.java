package com.lianjinsoft.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.lianjinsoft.util.LogUtil;

@Component
@RabbitListener(queues = "notify.payment")
public class PaymentNotifyReceive {
	@RabbitHandler
    public void receive(String msg) {
		LogUtil.info("notify.payment receive message: "+msg);
    }
}
