package com.lianjinsoft.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.lianjinsoft.util.LogUtil;

@Component
public class ApiCreditReceive {
	@RabbitHandler
	@RabbitListener(queues = "credit.bank")
	public void creditBank(String msg) {
		LogUtil.info("credit.bank receive message: "+msg);
	}
	
	@RabbitHandler
	@RabbitListener(queues = "credit.finance")
	public void creditFinance(String msg) {
		LogUtil.info("credit.finance receive message: "+msg);
	}
}
