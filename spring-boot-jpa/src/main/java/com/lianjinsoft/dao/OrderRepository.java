package com.lianjinsoft.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.lianjinsoft.pojo.Order;

public interface OrderRepository extends JpaRepository<Order, Integer>{
	@Transactional
	@Modifying
	@Query("update Order o set o.orderName = ?1 where o.id = ?2")
	int modifyOrder(String orderName, Integer id);
	
	Order findByOrderNo(String orderNo);
	
	List<Order> findByOrderNameStartingWith(String orderName);
	
	List<Order> findByAmountAndOrderName(BigDecimal amount, String orderName);
	
	Page<Order> findByAmount(BigDecimal amount, Pageable page);
}
