package com.lianjinsoft.dao;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.lianjinsoft.pojo.Order;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderRepositoryTests {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private OrderRepository orderRps;
	
	@Test
	public void base_add(){
		//测试jpa默认封装insert方法
		Order order = orderRps.save(
				new Order(String.valueOf(System.currentTimeMillis()), "订单名称", new BigDecimal(100)));
		Assert.assertNotNull(order);
		Assert.assertNotNull(order.getId());
	}
	
	@Test
	public void base_delete(){
		//测试jpa默认封装delete方法
		//为保证此案例执行时有数据，增加校验逻辑
		List<Order> list = orderRps.findAll();
		if(CollectionUtils.isEmpty(list)){
			base_add();
			base_delete();
			return;
		}
		
		//获取列表中第一个订单
		Order order = list.get(0);
		orderRps.delete(order);
		
		//删除成功后，再次查询
		order = orderRps.findOne(order.getId());
		Assert.assertNull(order);
	}
	
	@Test
	public void base_getAll(){
		//测试jpa默认封装select方法
		List<Order> list = orderRps.findAll();
		logger.info(list.toString());
		Assert.assertNotNull(list);
	}
	
	@Test
	public void zdy_update(){
		//测试自定义update方法
		//为保证此案例执行时有数据，增加校验逻辑
		List<Order> list = orderRps.findAll();
		if(CollectionUtils.isEmpty(list)){
			base_add();
			zdy_update();
			return;
		}
		
		//获取列表中第一个订单
		Order order = list.get(0);
		
		//修改订单名称
		String orderName = "订单"+System.currentTimeMillis();
		int count = orderRps.modifyOrder(orderName, order.getId());
		
		//验证修改结果
		Assert.assertEquals(count, 1);
		
		//修改成功后，再次查询订单信息
		order = orderRps.findOne(order.getId());
		Assert.assertEquals(orderName, order.getOrderName());
	}
	
	@Test
	public void zdy_query1(){
		//测试自定义select方法（jpa自动增加查询条件）
		//为保证此案例执行时有数据，增加校验逻辑
		List<Order> list = orderRps.findAll();
		if(CollectionUtils.isEmpty(list)){
			base_add();
			zdy_query1();
			return;
		}
		
		Order order = orderRps.findByOrderNo(list.get(0).getOrderNo());
		logger.info(order.toString());
		Assert.assertNotNull(order);
	}
	
	@Test
	public void zdy_query2(){
		//测试自定义select方法（jpa自动增加like条件）
		List<Order> list = orderRps.findByOrderNameStartingWith("订单");
		logger.info(list.toString());
		Assert.assertNotNull(list);
	}
	
	@Test
	public void zdy_query3(){
		//测试自定义select方法（jpa自动增加多个查询条件）
		List<Order> list = orderRps.findByAmountAndOrderName(new BigDecimal(100), "订单名称");
		logger.info(list.toString());
		Assert.assertNotNull(list);
	}
	
	@Test
	public void zdy_query4(){
		//测试自定义select方法（jpa支持分页查询）
		int page = 1;//第几页（分页从0开始）
		int size = 10;//每页返回条数
		Sort sort = new Sort(Direction.DESC, "addTime");
		Pageable pg = new PageRequest(page, size, sort);
		Page<Order> list = orderRps.findByAmount(new BigDecimal(100), pg);
		logger.info(ToStringBuilder.reflectionToString(list));
		Assert.assertNotNull(list);
	}
}
