package com.lianjinsoft.controller;

import java.math.BigDecimal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculateController {
	
	@RequestMapping(value = "/add")
	public BigDecimal add(BigDecimal num1, BigDecimal num2){
		if(num1==null||num2==null){
			return BigDecimal.ZERO;
		}
		
		return num1.add(num2);
	}
	
}
