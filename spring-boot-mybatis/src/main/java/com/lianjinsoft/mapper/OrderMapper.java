package com.lianjinsoft.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.lianjinsoft.pojo.Order;

public interface OrderMapper {
	@Insert("INSERT INTO orders(order_no,order_name,amount,add_time) "
			+ "VALUES(#{orderNo}, #{orderName}, #{amount}, #{addTime})")
	void addOrder(Order order);
	
	@Delete("DELETE FROM orders WHERE id=#{id}")
	void delOrder(Integer id);
	
	@Update("UPDATE orders SET order_name=#{orderName} WHERE id=#{id}")
	void updOrder(Order order);

	@Select("SELECT * FROM orders")
	@Results({
		@Result(property = "orderNo",  column = "order_no"),
		@Result(property = "orderName", column = "order_name"),
		@Result(property = "addTime", column = "add_time"),
	})
	List<Order> queryOrders();
	
	@Select("SELECT * FROM orders WHERE order_no=#{orderNo}")
	@Results({
		@Result(property = "orderNo",  column = "order_no"),
		@Result(property = "orderName", column = "order_name"),
		@Result(property = "addTime", column = "add_time"),
	})
	Order queryOrderByNo(String orderNo);
}
