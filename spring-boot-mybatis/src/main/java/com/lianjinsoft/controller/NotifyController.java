package com.lianjinsoft.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotifyController {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping("/notify")
	public String addOrder(HttpServletRequest request){
		Map reqMap = new HashMap();
		Enumeration<String> names = request.getParameterNames();
		while(names.hasMoreElements()){
			String name = names.nextElement();
			reqMap.put(name, request.getParameter(name));
		}
		
		logger.info(reqMap.toString());
		return "success";
	}
}
