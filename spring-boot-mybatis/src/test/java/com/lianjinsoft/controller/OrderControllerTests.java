package com.lianjinsoft.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderControllerTests {
	@Autowired
    private WebApplicationContext context;
	private MockMvc mvc;
	
	@Before
    public void init() throws Exception {
		//MockMvc注入spring容器，否则controller中注入的service、mapper均会报空指针
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }
	
	//测试OrderController.add()方法
	@Test
    public void add() throws Exception {
		//模拟请求参数
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    	params.add("orderNo", String.valueOf(System.currentTimeMillis()));
    	params.add("orderName", "订单名称");
    	params.add("amount", "100");
		
    	//通过MockMvc模拟post表单提交
        mvc.perform(post("/order/add").accept(MediaType.APPLICATION_JSON).params(params))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
    }
	
	//测试OrderController.queryAll()方法
	@Test
	public void queryAll() throws Exception {
		//通过MockMvc模拟post表单提交
		mvc.perform(post("/order/queryAll").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andDo(print())
		.andReturn();
	}
	
}
