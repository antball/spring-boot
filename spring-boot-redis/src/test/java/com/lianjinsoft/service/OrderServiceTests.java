package com.lianjinsoft.service;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lianjinsoft.pojo.Order;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTests {
	@Autowired
	private OrderService orderService;

	@Test
	public void test1_add() {
		//添加订单(注意：执行该方法先执行删除方法或修改订单号，否则会出现订单号重复异常)
		orderService.addOrder(new Order("100001", "XX手机", new BigDecimal("4999")));
	}
	
	@Test
	public void test2_query() {
		//查询订单
		orderService.queryOrderByNo("100001");
	}
	
	@Test
	public void test3_update() {
		//先查询订单信息
		Order order = orderService.queryOrderByNo("100001");
		order.setOrderName("国民手机");
		
		//更新DB及缓存
		orderService.updOrder(order);
		
		//再次查询
		orderService.queryOrderByNo("100001");
	}
	
	@Test
	public void test4_del() {
		//删除DB及缓存
		orderService.delOrder("100001");
		
		//再次查询
		orderService.queryOrderByNo("100001");
	}
}
