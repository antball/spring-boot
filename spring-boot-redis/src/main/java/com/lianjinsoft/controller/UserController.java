package com.lianjinsoft.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lianjinsoft.util.LogUtil;

@RestController
public class UserController {
	@RequestMapping("/login")
	public String login(HttpServletRequest req){
		String sessionId = req.getSession().getId();
		LogUtil.info("sessionId="+sessionId);
		return sessionId;
	}
}
