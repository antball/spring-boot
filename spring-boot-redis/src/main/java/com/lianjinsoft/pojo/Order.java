package com.lianjinsoft.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class Order {
	private Integer id;
	private String orderNo;
	private String orderName;
	private BigDecimal amount;
	private Date addTime;
	
	public Order(){}
	
	public Order(String orderNo, String orderName, BigDecimal amount){
		this.orderNo = orderNo;
		this.orderName = orderName;
		this.amount = amount;
		this.addTime = new Date();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getOrderName() {
		return orderName;
	}
	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	
	@Override
	public String toString() {
		return "Order [id=" + id + ", orderNo=" + orderNo + ", orderName="
				+ orderName + ", amount=" + amount + ", addTime=" + addTime
				+ "]";
	}
	
}
