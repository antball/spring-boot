package com.lianjinsoft.mapper;

import com.lianjinsoft.pojo.Order;

public interface OrderMapper {
	void addOrder(Order order);
	
	void delOrder(String orderNo);
	
	void updOrder(Order order);
	
	Order queryOrderByNo(String orderNo);
}
