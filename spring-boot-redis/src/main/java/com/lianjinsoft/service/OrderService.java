package com.lianjinsoft.service;

import com.lianjinsoft.pojo.Order;

public interface OrderService {
	void addOrder(Order order);
	
	void delOrder(String orderNo);
	
	void updOrder(Order order);
	
	Order queryOrderByNo(String orderNo);
}
